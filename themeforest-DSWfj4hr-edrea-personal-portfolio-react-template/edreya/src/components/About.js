import { Fragment, useState } from "react";
import AnimatedText from "./AnimatedText";
import AboutPopup from "./popup/AboutPopup";
import Image from 'next/image';

const aboutData = {
  firstName: "Chris",
  lastName: "Zambito",
  bithday: "11.17.1993",
  address: "Buffalo NY",
  phn: "+1 716 698 8355",
  email: "c.r.zambito@gmail.com",
  serviceLists: [
    "Website Development",
    "Digital Experience",
    "Content Marketing",
    "Social Media Design",
    "Shared Web Hosting",
  ],
  skills: {
    programming: [
      { name: "Python", value: "95" },
      { name: "JavaScript", value: "80" },
      { name: "TypeScript", value: "90" },
      { name: "SQL", value: "95" },
      { name: "HTML", value: "80" },
      { name: "CSS", value: "90" },
      { name: "React", value: "95" },
      { name: "React Native", value: "80" },
      { name: "Swift", value: "90" },
      { name: "Bootstrap", value: "95" },
      { name: "TailWind", value: "80" },
      { name: "Django", value: "90" },
      { name: "FastAPI", value: "95" },
      { name: "JavaScript", value: "80" },
      { name: "Angular", value: "90" },
      { name: "WordPress", value: "95" },
      { name: "JavaScript", value: "80" },
      { name: "Angular", value: "90" },
    ],
    language: [
      { name: "React", value: "95" },
      { name: "Django", value: "80" },
      { name: "FastAPI", value: "90" },
    ],
  },
  education: [
    { year: "2023", unv: "Hack Reactor", degree: "Advanced Software Engineering" },
    { year: "2022", unv: "Lingoda GmbH", degree: "German Language and Literature" },
    { year: "2016", unv: "Buffalo State University", degree: "Business Administration, B.S." },
  ],
  working: [
    {
      year: "2023 - running",
      company: "Apex Cloud Development",
      deg: "Associate Software Engineer - Intern",
    },
    { year: "2009 - 2023", company: "Wesselmann Dry Cleaners", deg: "Operational Manager" },
    { year: "2009 - 2023", company: "BridalKare Gown Preservation", deg: "Operational Manager" },
    { year: "2009 - 2023", company: "CRDN of WNY [Textiles]", deg: "Operational Manager" },
    { year: "2009 - 2023", company: "CRDN of WNY [Electronics]", deg: "Operational Manager" },
  ],
  partnersLogos: [
    "img/partners/1.png",
    "img/partners/2.png",
    "img/partners/3.png",
    "img/partners/4.png",
  ],
};

const About = () => {
  const [popup, setPopup] = useState(false);
  return (
    <Fragment>
      <AboutPopup
        open={popup}
        close={() => setPopup(false)}
        aboutData={aboutData}
      />
      <div className="edrea_tm_section hidden animated" id="about">
        <div className="section_inner">
          <div className="edrea_tm_about">
            <div className="left">
              <div>
                <div>
                <Image src="/img/logo/portfoliologo3.png" alt="" height="320px" width="320px" />
                </div>
              </div>
            </div>
            <div className="right">
              <div className="short">
                <h3 className="name">
                  {aboutData.firstName}{" "}
                  <span className="coloring">{aboutData.lastName}</span>
                </h3>
                <h3 className="job">
                  <AnimatedText />
                </h3>
              </div>
              <div className="text">
                <p>
                  My name is <span>Chris Zambito.</span> I am a dedicated and passionate full-stack
                  software engineer with a unique background in family business ownership. After
                  successfully managing and operating several businesses for over a decade, I decided
                  to pursue my true passion and transitioned into software engineering. With 15 years
                  of professional experience as a graphic designer and now a software engineer, I have
                  acquired a diverse skill set that includes proficiency in a variety of programming
                  languages, front-end and back-end frameworks, and system design methodologies. I am
                  committed to delivering high-quality applications that are visually appealing, secure,
                  and performant. I am excited to bring my skills and experience to new challenges and
                  opportunities, and to continue to learn and grow as a software engineer.
                </p>
              </div>
              <div className="edrea_tm_button">
                <a href="#" onClick={() => setPopup(true)}>
                  Learn More
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
export default About;







// import { Fragment, useState } from "react";
// import AnimatedText from "./AnimatedText";
// import AboutPopup from "./popup/AboutPopup";
// import Image from 'next/image';

// const aboutData = {
//   firstName: "Chris",
//   lastName: "Zambito",
//   bithday: "11.17.1993",
//   address: "Buffalo NY",
//   phn: "+1 716 698 8355",
//   email: "c.r.zambito@gmail.com",
//   serviceLists: [
//     "Website Development",
//     "Digital Experience",
//     "Content Marketing",
//     "Social Media Design",
//     "Shared Web Hosting",
//   ],
//   skills: {
//     programming: [
//       { name: "Python", value: "95" },
//       { name: "JavaScript", value: "80" },
//       { name: "TypeScript", value: "90" },
//       { name: "SQL", value: "95" },
//       { name: "HTML", value: "80" },
//       { name: "CSS", value: "90" },
//       { name: "React", value: "95" },
//       { name: "React Native", value: "80" },
//       { name: "Swift", value: "90" },
//       { name: "Bootstrap", value: "95" },
//       { name: "TailWind", value: "80" },
//       { name: "Django", value: "90" },
//       { name: "FastAPI", value: "95" },
//       { name: "JavaScript", value: "80" },
//       { name: "Angular", value: "90" },
//       { name: "WordPress", value: "95" },
//       { name: "JavaScript", value: "80" },
//       { name: "Angular", value: "90" },
//     ],
//     language: [
//       { name: "React", value: "95" },
//       { name: "Django", value: "80" },
//       { name: "FastAPI", value: "90" },
//     ],
//   },
//   education: [
//     { year: "2023", unv: "Hack Reactor", degree: "Advanced Software Engineering" },
//     { year: "2022", unv: "Lingoda GmbH", degree: "German Language and Literature" },
//     { year: "2016", unv: "Buffalo State University", degree: "Business Administration, B.S." },
//   ],
//   working: [
//     {
//       year: "2023 - running",
//       company: "Apex Cloud Development",
//       deg: "Associate Software Engineer - Intern",
//     },
//     { year: "2009 - 2023", company: "Wesselmann Dry Cleaners", deg: "Operational Manager" },
//     { year: "2009 - 2023", company: "BridalKare Gown Preservation", deg: "Operational Manager" },
//     { year: "2009 - 2023", company: "CRDN of WNY [Textiles]", deg: "Operational Manager" },
//     { year: "2009 - 2023", company: "CRDN of WNY [Electronics]", deg: "Operational Manager" },
//   ],
//   partnersLogos: [
//     "img/partners/1.png",
//     "img/partners/2.png",
//     "img/partners/3.png",
//     "img/partners/4.png",
//   ],
// };

// const About = () => {
//   const [popup, setPopup] = useState(false);
//   return (
//     <Fragment>
//       <AboutPopup
//         open={popup}
//         close={() => setPopup(false)}
//         aboutData={aboutData}
//       />
//       <div className="edrea_tm_section hidden animated" id="about">
//         <div className="section_inner">
//           <div className="edrea_tm_about">
//             <div className="left">
//               <div className="image">
//                 <img src="img/thumbs/1-1.jpg" alt="" />
//                 <div className="main" data-img-url="img/about/1.jpg" />
//               </div>
//                 {/* <div className="main" data-img-url="https://gitlab.com/uploads/-/system/user/avatar/13610005/avatar.png?width=400" /> */}
//             </div>
//             <div className="right">
//               <div className="short">
//                 <h3 className="name">
//                   {aboutData.firstName}{" "}
//                   <span className="coloring">{aboutData.lastName}</span>
//                 </h3>
//                 <h3 className="job">
//                   <AnimatedText />
//                 </h3>
//               </div>
//               <div className="text">
//                 <p>
//                   My name is <span>Chris Zambito.</span> I am a dedicated and passionate full-stack
//                   software engineer with a unique background in family business ownership. After
//                   successfully managing and operating several businesses for over a decade, I decided
//                   to pursue my true passion and transitioned into software engineering. With 15 years
//                   of professional experience as a graphic designer and now a software engineer, I have
//                   acquired a diverse skill set that includes proficiency in a variety of programming
//                   languages, front-end and back-end frameworks, and system design methodologies. I am
//                   committed to delivering high-quality applications that are visually appealing, secure,
//                   and performant. I am excited to bring my skills and experience to new challenges and
//                   opportunities, and to continue to learn and grow as a software engineer.
//                 </p>
//               </div>
//               <div className="edrea_tm_button">
//                 <a href="#" onClick={() => setPopup(true)}>
//                   Learn More
//                 </a>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </Fragment>
//   );
// };
// export default About;
