import { useContext } from "react";
import { buildStyles, CircularProgressbar } from "react-circular-progressbar";
import { Swiper, SwiperSlide } from "swiper/react";
import { context } from "../../context/context";
import { testimonialSlider } from "../../sliderProps";
import AnimatedText from "../AnimatedText";
import Modal from "./Modal";
import Image from "next/image";

const AboutPopup = ({ open, close, aboutData }) => {
  const colorContext = useContext(context);
  const { color } = colorContext;

  return (
    <Modal open={open} close={close}>
      <div className="about_popup_details">
        <div className="left">
          <div className="left_inner">
            <div className="author">
              <div>
                <Image src="/img/logo/portfoliologo.png" alt="" width={657} height={664} />
              </div>
              <div className="short">
                <h3 className="name">
                  {aboutData.firstName}{" "}
                  <span className="coloring">{aboutData.lastName}</span>
                </h3>
                <h3 className="job">
                  <AnimatedText />
                </h3>
              </div>
            </div>
            <div className="list">
              <ul>
                <li>
                  <div className="list_inner">
                    <i className="icon-user" />
                    <span>
                      {aboutData.firstName} {aboutData.lastName}
                    </span>
                  </div>
                </li>
                <li>
                  <div className="list_inner">
                    <i className="icon-calendar" />
                    <span>{aboutData.bithday}</span>
                  </div>
                </li>
                <li>
                  <div className="list_inner">
                    <i className="icon-location" />
                    <span>
                      <a href="#" className="href_location">
                        {aboutData.address}
                      </a>
                    </span>
                  </div>
                </li>
                <li>
                  <div className="list_inner">
                    <i className="icon-phone" />
                    <span>
                      <a href="#">{aboutData.phn}</a>
                    </span>
                  </div>
                </li>
                <li>
                  <div className="list_inner">
                    <i className="icon-mail-1" />
                    <span>
                      <a href={`mailto:${aboutData.email}`}>
                        {aboutData.email}
                      </a>
                    </span>
                  </div>
                </li>
              </ul>
            </div>
            <div className="edrea_tm_button full">
              <a href="img/about/Chris-Zambito-Resume.docx (2).pdf" download>
                Download CV
              </a>
            </div>
          </div>
        </div>
        <div className="right">
          <div className="right_inner">
            <div className="biography">
              <div className="about_title">
                <h3>
                  <span>
                    About <span className="coloring">Me</span>
                  </span>
                </h3>
              </div>
              <div className="text">
                <p>
                  My name is <span>Chris Zambito.</span> I am a dedicated and passionate full-stack
                  software engineer with a unique background in family business ownership. After
                  successfully managing and operating several businesses for over a decade, I decided
                  to pursue my true passion and transitioned into software engineering. With 15 years
                  of professional experience as a graphic designer and now a software engineer, I have
                  acquired a diverse skill set that includes proficiency in a variety of programming
                  languages, front-end and back-end frameworks, and system design methodologies. I am
                  committed to delivering high-quality applications that are visually appealing, secure,
                  and performant. I am excited to bring my skills and experience to new challenges and
                  opportunities, and to continue to learn and grow as a software engineer.
                </p>
              </div>
            </div>
            <div className="service">
              <div className="about_title">
                <h3>
                  <span>
                    Quality <span className="coloring">Services</span>
                  </span>
                </h3>
              </div>
              <div className="list">
                <ul>
                  {aboutData.serviceLists &&
                    aboutData.serviceLists.map((service, i) => (
                      <li key={i}>
                        <i className="icon-right-dir" />
                        {service}
                      </li>
                    ))}
                </ul>
              </div>
            </div>
            <div className="prog_skill">
              <div className="about_title">
                <h3>
                  <span>
                    Programming <span className="coloring">Skills</span>
                  </span>
                </h3>
              </div>
              <div className="oki_progress">
                {aboutData.skills &&
                  aboutData.skills.programming &&
                  aboutData.skills.programming.map((programming, i) => (
                    <div
                      key={i}
                      className="progress_inner skillsInner___"
                      data-value={95}
                    >
                      <span>
                        <span className="label">{programming.name}</span>
                        <span
                          className="number"
                          style={{ right: `${100 - programming.value}%` }}
                        >
                          {programming.value}%
                        </span>
                      </span>
                      <div className="background">
                        <div className="bar open">
                          <div
                            className="bar_in"
                            style={{ width: `${programming.value}%` }}
                          />
                        </div>
                      </div>
                    </div>
                  ))}
              </div>
            </div>
            <div className="lang_skill">
              <div className="about_title">
                <h3>
                  <span>
                    Language <span className="coloring">Skills</span>
                  </span>
                </h3>
              </div>
              <div className="circular_progress_bar">
                <ul>
                  {aboutData.skills &&
                    aboutData.skills.language &&
                    aboutData.skills.language.map((language, i) => (
                      <li key={i}>
                        <div className="list_inner">
                          <div className="myCircle" data-value="0.95">
                            <CircularProgressbar
                              value={language.value}
                              text={`${language.value}%`}
                              strokeWidth={2}
                              styles={buildStyles({
                                // Colors
                                pathColor: color,
                              })}
                            />
                          </div>
                          <div className="title">
                            <span>{language.name}</span>
                          </div>
                        </div>
                      </li>
                    ))}
                </ul>
              </div>
            </div>
            <div className="timeline">
              <div className="about_title">
                <h3>
                  <span>
                    Education <span className="coloring">Timeline</span>
                  </span>
                </h3>
              </div>
              <div className="list">
                <ul>
                  {aboutData &&
                    aboutData.education &&
                    aboutData.education.map((edu, i) => (
                      <li key={i}>
                        <div className="list_inner">
                          <div className="time">
                            <span>{edu.year}</span>
                          </div>
                          <div className="place">
                            <h3>{edu.unv}</h3>
                            <span>{edu.degree}</span>
                          </div>
                        </div>
                      </li>
                    ))}
                </ul>
              </div>
            </div>
            <div className="timeline">
              <div className="about_title">
                <h3>
                  <span>
                    Working <span className="coloring">Timeline</span>
                  </span>
                </h3>
              </div>
              <div className="list">
                <ul>
                  {aboutData &&
                    aboutData.working &&
                    aboutData.working.map((work, i) => (
                      <li key={i}>
                        <div className="list_inner">
                          <div className="time">
                            <span>{work.year}</span>
                          </div>
                          <div className="place">
                            <h3>{work.company}</h3>
                            <span>{work.deg}</span>
                          </div>
                        </div>
                      </li>
                    ))}
                </ul>
              </div>
            </div>
            <div className="partners">
              <div className="about_title">
                <h3>
                  <span>
                    My <span className="coloring">Partners</span>
                  </span>
                </h3>
              </div>
              <div className="list">
                <ul>
                  {aboutData &&
                    aboutData.partnersLogos &&
                    aboutData.partnersLogos.map((logo, i) => (
                      <li key={i}>
                        <div className="list_inner">
                          <Image src={"/" + logo} alt="" width={100} height={100} />
                          <a className="cavani_tm_full_link" href="#" />
                        </div>
                      </li>
                    ))}
                </ul>
              </div>
            </div>
            <div className="testimonial">
              <div className="about_title">
                <h3>
                  <span>
                    Reccomendations <span className="coloring">Received</span>
                  </span>
                </h3>
              </div>
              <div className="list">
                <Swiper {...testimonialSlider} className="owl-carousel">
                  <SwiperSlide>
                    <div className="list_inner">
                      <div className="text">
                        <i className="icon-quote-left" />
                        <p>
                        I am delighted to recommend Chris as a capable and dedicated colleague. During our time together at Hack Reactor, Chris consistently proved his strong work ethic, often going the extra mile to ensure tasks were completed thoroughly and effectively. Chris is a great problem solver showing a strong adaptability to new technologies and techniques. In conclusion, Chris would make a valuable addition to any team, combining hard work, adaptability, and excellent communication skills. I am confident he will succeed in any future roles he takes on.
                        </p>
                      </div>
                      <div className="details">
                        <div className="image">
                          <div
                            className="main"
                            data-img-url="img/testimonials/1.jpg"
                          />
                        </div>
                        <div className="info">
                          <h3>Shahem Al Hadid</h3>
                          <span>Software Engineer</span>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div className="list_inner">
                      <div className="text">
                        <i className="icon-quote-left" />
                        <p>
                        I wholeheartedly endorse Chris for any upcoming opportunities. Having had the privilege of collaborating with Chris during our time at the Hack Reactor Bootcamp, I was consistently amazed by their capabilities and unwavering dedication. Chris possesses exceptional teamwork skills, consistently going above and beyond to assist and uplift their teammates. They have a remarkable ability to grasp new concepts swiftly and exhibit meticulous attention to detail, making them an invaluable contributor to any project. I am confident that Chris will surpass expectations and thrive in all their future endeavors.
                        </p>
                      </div>
                      <div className="details">
                        <div className="image">
                          <div
                            className="main"
                            data-img-url="img/testimonials/2.jpg"
                          />
                        </div>
                        <div className="info">
                          <h3>Jin Park</h3>
                          <span>Software Engineer</span>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div className="list_inner">
                      <div className="text">
                        <i className="icon-quote-left" />
                        <p>
                        Based on his exceptional skills, extensive knowledge, and strong leadership abilities, Chris is an outstanding Software Engineer who excels in team settings. His unwavering determination, resilience, and commitment to delivering high-quality work make him a valuable asset to any project. With his remarkable expertise and dependable nature, I highly recommend Chris for any company
                        </p>
                      </div>
                      <div className="details">
                        <div className="image">
                          <div
                            className="main"
                            data-img-url="img/testimonials/3.jpg"
                          />
                        </div>
                        <div className="info">
                          <h3>Liland Pham</h3>
                          <span>Software Engineer</span>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                </Swiper>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Modal>
  );
};
export default AboutPopup;




// import { useContext } from "react";
// import { buildStyles, CircularProgressbar } from "react-circular-progressbar";
// import { Swiper, SwiperSlide } from "swiper/react";
// import { context } from "../../context/context";
// import { testimonialSlider } from "../../sliderProps";
// import AnimatedText from "../AnimatedText";
// import Modal from "./Modal";

// const AboutPopup = ({ open, close, aboutData }) => {
//   const colorContext = useContext(context);
//   const { color } = colorContext;

//   return (
//     <Modal open={open} close={close}>
//       <div className="about_popup_details">
//         <div className="left">
//           <div className="left_inner">
//             <div className="author">
//               <div className="avatar_image">
//                 <img src="img/thumbs/1-1.jpg" alt="" />
//                 {/* <div className="main" data-img-url="https://gitlab.com/uploads/-/system/user/avatar/13610005/avatar.png?width=400" /> */}
//                 <div className="main" data-img-url="img/about/1.jpg" />
//               </div>
//               <div className="short">
//                 <h3 className="name">
//                   {aboutData.firstName}{" "}
//                   <span className="coloring">{aboutData.lastName}</span>
//                 </h3>
//                 <h3 className="job">
//                   <AnimatedText />
//                 </h3>
//               </div>
//             </div>
//             <div className="list">
//               <ul>
//                 <li>
//                   <div className="list_inner">
//                     <i className="icon-user" />
//                     <span>
//                       {aboutData.firstName} {aboutData.lastName}
//                     </span>
//                   </div>
//                 </li>
//                 <li>
//                   <div className="list_inner">
//                     <i className="icon-calendar" />
//                     <span>{aboutData.bithday}</span>
//                   </div>
//                 </li>
//                 <li>
//                   <div className="list_inner">
//                     <i className="icon-location" />
//                     <span>
//                       <a href="#" className="href_location">
//                         {aboutData.address}
//                       </a>
//                     </span>
//                   </div>
//                 </li>
//                 <li>
//                   <div className="list_inner">
//                     <i className="icon-phone" />
//                     <span>
//                       <a href="#">{aboutData.phn}</a>
//                     </span>
//                   </div>
//                 </li>
//                 <li>
//                   <div className="list_inner">
//                     <i className="icon-mail-1" />
//                     <span>
//                       <a href={`mailto:${aboutData.email}`}>
//                         {aboutData.email}
//                       </a>
//                     </span>
//                   </div>
//                 </li>
//               </ul>
//             </div>
//             <div className="edrea_tm_button full">
//               <a href="img/about/Chris-Zambito-Resume.docx (2).pdf" download>
//                 Download CV
//               </a>
//             </div>
//           </div>
//         </div>
//         <div className="right">
//           <div className="right_inner">
//             <div className="biography">
//               <div className="about_title">
//                 <h3>
//                   <span>
//                     About <span className="coloring">Me</span>
//                   </span>
//                 </h3>
//               </div>
//               <div className="text">
//                 <p>
//                   My name is <span>Chris Zambito.</span> I am a dedicated and passionate full-stack
//                   software engineer with a unique background in family business ownership. After
//                   successfully managing and operating several businesses for over a decade, I decided
//                   to pursue my true passion and transitioned into software engineering. With 15 years
//                   of professional experience as a graphic designer and now a software engineer, I have
//                   acquired a diverse skill set that includes proficiency in a variety of programming
//                   languages, front-end and back-end frameworks, and system design methodologies. I am
//                   committed to delivering high-quality applications that are visually appealing, secure,
//                   and performant. I am excited to bring my skills and experience to new challenges and
//                   opportunities, and to continue to learn and grow as a software engineer.
//                 </p>
//               </div>
//             </div>
//             <div className="service">
//               <div className="about_title">
//                 <h3>
//                   <span>
//                     Quality <span className="coloring">Services</span>
//                   </span>
//                 </h3>
//               </div>
//               <div className="list">
//                 <ul>
//                   {aboutData.serviceLists &&
//                     aboutData.serviceLists.map((service, i) => (
//                       <li key={i}>
//                         <i className="icon-right-dir" />
//                         {service}
//                       </li>
//                     ))}
//                 </ul>
//               </div>
//             </div>
//             <div className="prog_skill">
//               <div className="about_title">
//                 <h3>
//                   <span>
//                     Programming <span className="coloring">Skills</span>
//                   </span>
//                 </h3>
//               </div>
//               <div className="oki_progress">
//                 {aboutData.skills &&
//                   aboutData.skills.programming &&
//                   aboutData.skills.programming.map((programming, i) => (
//                     <div
//                       key={i}
//                       className="progress_inner skillsInner___"
//                       data-value={95}
//                     >
//                       <span>
//                         <span className="label">{programming.name}</span>
//                         <span
//                           className="number"
//                           style={{ right: `${100 - programming.value}%` }}
//                         >
//                           {programming.value}%
//                         </span>
//                       </span>
//                       <div className="background">
//                         <div className="bar open">
//                           <div
//                             className="bar_in"
//                             style={{ width: `${programming.value}%` }}
//                           />
//                         </div>
//                       </div>
//                     </div>
//                   ))}
//               </div>
//             </div>
//             <div className="lang_skill">
//               <div className="about_title">
//                 <h3>
//                   <span>
//                     Language <span className="coloring">Skills</span>
//                   </span>
//                 </h3>
//               </div>
//               <div className="circular_progress_bar">
//                 <ul>
//                   {aboutData.skills &&
//                     aboutData.skills.language &&
//                     aboutData.skills.language.map((language, i) => (
//                       <li key={i}>
//                         <div className="list_inner">
//                           <div className="myCircle" data-value="0.95">
//                             <CircularProgressbar
//                               value={language.value}
//                               text={`${language.value}%`}
//                               strokeWidth={2}
//                               styles={buildStyles({
//                                 // Colors
//                                 pathColor: color,
//                               })}
//                             />
//                           </div>
//                           <div className="title">
//                             <span>{language.name}</span>
//                           </div>
//                         </div>
//                       </li>
//                     ))}
//                 </ul>
//               </div>
//             </div>
//             <div className="timeline">
//               <div className="about_title">
//                 <h3>
//                   <span>
//                     Education <span className="coloring">Timeline</span>
//                   </span>
//                 </h3>
//               </div>
//               <div className="list">
//                 <ul>
//                   {aboutData &&
//                     aboutData.education &&
//                     aboutData.education.map((edu, i) => (
//                       <li key={i}>
//                         <div className="list_inner">
//                           <div className="time">
//                             <span>{edu.year}</span>
//                           </div>
//                           <div className="place">
//                             <h3>{edu.unv}</h3>
//                             <span>{edu.degree}</span>
//                           </div>
//                         </div>
//                       </li>
//                     ))}
//                 </ul>
//               </div>
//             </div>
//             <div className="timeline">
//               <div className="about_title">
//                 <h3>
//                   <span>
//                     Working <span className="coloring">Timeline</span>
//                   </span>
//                 </h3>
//               </div>
//               <div className="list">
//                 <ul>
//                   {aboutData &&
//                     aboutData.working &&
//                     aboutData.working.map((work, i) => (
//                       <li key={i}>
//                         <div className="list_inner">
//                           <div className="time">
//                             <span>{work.year}</span>
//                           </div>
//                           <div className="place">
//                             <h3>{work.company}</h3>
//                             <span>{work.deg}</span>
//                           </div>
//                         </div>
//                       </li>
//                     ))}
//                 </ul>
//               </div>
//             </div>
//             <div className="partners">
//               <div className="about_title">
//                 <h3>
//                   <span>
//                     My <span className="coloring">Partners</span>
//                   </span>
//                 </h3>
//               </div>
//               <div className="list">
//                 <ul>
//                   {aboutData &&
//                     aboutData.partnersLogos &&
//                     aboutData.partnersLogos.map((logo, i) => (
//                       <li key={i}>
//                         <div className="list_inner">
//                           <img src={logo} alt="" />
//                           <a className="cavani_tm_full_link" href="#" />
//                         </div>
//                       </li>
//                     ))}
//                 </ul>
//               </div>
//             </div>
//             <div className="testimonial">
//               <div className="about_title">
//                 <h3>
//                   <span>
//                     Reccomendations <span className="coloring">Received</span>
//                   </span>
//                 </h3>
//               </div>
//               <div className="list">
//                 <Swiper {...testimonialSlider} className="owl-carousel">
//                   <SwiperSlide>
//                     <div className="list_inner">
//                       <div className="text">
//                         <i className="icon-quote-left" />
//                         <p>
//                         I'm delighted to recommend Chris as a capable and dedicated colleague. During our time together at Hack Reactor, Chris consistently proved his strong work ethic, often going the extra mile to ensure tasks were completed thoroughly and effectively. Chris is a great problem solver showing a strong adaptability to new technologies and techniques. In conclusion, Chris would make a valuable addition to any team, combining hard work, adaptability, and excellent communication skills. I am confident he will succeed in any future roles he takes on.
//                         </p>
//                       </div>
//                       <div className="details">
//                         <div className="image">
//                           <div
//                             className="main"
//                             data-img-url="img/testimonials/1.jpg"
//                           />
//                         </div>
//                         <div className="info">
//                           <h3>Shahem Al Hadid</h3>
//                           <span>Software Engineer</span>
//                         </div>
//                       </div>
//                     </div>
//                   </SwiperSlide>
//                   <SwiperSlide>
//                     <div className="list_inner">
//                       <div className="text">
//                         <i className="icon-quote-left" />
//                         <p>
//                         I wholeheartedly endorse Chris for any upcoming opportunities. Having had the privilege of collaborating with Chris during our time at the Hack Reactor Bootcamp, I was consistently amazed by their capabilities and unwavering dedication. Chris possesses exceptional teamwork skills, consistently going above and beyond to assist and uplift their teammates. They have a remarkable ability to grasp new concepts swiftly and exhibit meticulous attention to detail, making them an invaluable contributor to any project. I am confident that Chris will surpass expectations and thrive in all their future endeavors.
//                         </p>
//                       </div>
//                       <div className="details">
//                         <div className="image">
//                           <div
//                             className="main"
//                             data-img-url="img/testimonials/2.jpg"
//                           />
//                         </div>
//                         <div className="info">
//                           <h3>Jin Park</h3>
//                           <span>Software Engineer</span>
//                         </div>
//                       </div>
//                     </div>
//                   </SwiperSlide>
//                   <SwiperSlide>
//                     <div className="list_inner">
//                       <div className="text">
//                         <i className="icon-quote-left" />
//                         <p>
//                         Based on his exceptional skills, extensive knowledge, and strong leadership abilities, Chris is an outstanding Software Engineer who excels in team settings. His unwavering determination, resilience, and commitment to delivering high-quality work make him a valuable asset to any project. With his remarkable expertise and dependable nature, I highly recommend Chris for any company
//                         </p>
//                       </div>
//                       <div className="details">
//                         <div className="image">
//                           <div
//                             className="main"
//                             data-img-url="img/testimonials/3.jpg"
//                           />
//                         </div>
//                         <div className="info">
//                           <h3>Liland Pham</h3>
//                           <span>Software Engineer</span>
//                         </div>
//                       </div>
//                     </div>
//                   </SwiperSlide>
//                 </Swiper>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </Modal>
//   );
// };
// export default AboutPopup;
